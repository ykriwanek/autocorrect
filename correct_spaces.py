#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
author: ykriwanek
Usage: correct_spaces.py <directory-with-xliffs-to-correct>
Output: in the input directory a "Leerzeichen_korrigiert" directory is created. The output-directory contains corrected
xliffs and a file corrections.txt, where corrections are listed.

The code takes as input a directoty with xliffs and changes the normal spaces between a number and a physical unit
into the no-break spaces.
"""

import os
from lxml import etree as ET
from tkinter.filedialog import askdirectory
from tkinter import *
import re

# xmlns="urn:oasis:names:tc:xliff:document:1.2"
# xmlns:ax="AcrossXliff"
# xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"

output = ""
correction_counter = 0
units_to_check = ['A', 'bar', 'Bit', 'Bq', 'BTU', 'Byte', 'C', 'cd', 'cm', 'd', 'dB', 'F', 'ft', 'g', 'Gal', 'Gy', 'h',
                  'H', 'ha', 'Hz',
                  'J', 'K', 'kBit', 'kg', 'kHz', 'km', 'kPa', 'kV', 'kW', 'kWh', 'kΩ', 'l', 'L', 'lm', 'lx', 'm', 'mA',
                  'mbar', 'mg',
                  'min', 'ml', 'mm', 'mmol', 'mol', 'Mol', 'ms', 'mS', 'N', 'Nm', 'Pa', 'rad', 's', 'S', 'sec', 'sr',
                  'Sv', 't', 'T',
                  'U', 'V', 'W', 'Wb', 'μS', 'Ω', '%', '°C', '°F']


def create_dir(dir_name):
    # define the name of the directory to be created
    path = dir_name
    try:
        os.mkdir(path)
    except OSError:
        print("Creation of the directory %s failed" % path)
    else:
        print("Successfully created the directory %s " % path)


def print_corrections():
    out_f = open(os.path.join(dirname, "Leerzeichen_korrigiert", 'corrections.txt'), 'w')
    print(output, file=out_f)
    out_f.close()

    """root = Tk()
    T = Text(root)
    T.pack()
    T.insert(END, output)
    mainloop()"""


def correct_spaces(file_path):
    tree = ET.parse(file_path)
    root = tree.getroot()

    body = root[0][1]
    global units_to_check
    global correction_counter
    global output

    for unit in units_to_check:
        for trans_unit in body.findall('{urn:oasis:names:tc:xliff:document:1.2}trans-unit'):
            target = trans_unit.find('{urn:oasis:names:tc:xliff:document:1.2}target')

            if target is not None:
                for child in target.iter():
                    if child.text is not None:
                        child.text = __correct_space(child.text, unit)
                    if child.tail is not None:
                        child.tail = __correct_space(child.tail, unit)

    output += "Insgesamt " + str(correction_counter) + " Korrekturen\n"
    path, filename = os.path.split(file_path)
    if correction_counter > 0:
        tree.write(os.path.join(path, "Leerzeichen_korrigiert", filename), encoding='UTF-8', xml_declaration=True)
    correction_counter = 0


def __correct_space(target_text, unit):
    global output
    global correction_counter

    corrected_text = re.subn("(\d+)[^\u00a0\S]*" + unit + r'\b', "\g<1>\u00a0" + unit, target_text)
    correction_counter += corrected_text[1]
    if corrected_text[1] > 0:
        output += "\n[" + target_text + "]:\n"
        for pat in re.findall("\d+[^\u00a0\S]*" + unit + r'\b', target_text):
            output += pat + " -> " + re.sub("(\d+)[^\u00a0\S]*" + unit + r'\b', "\g<1>\u00a0" + unit, pat) + "\n"

    return corrected_text[0]


if __name__ == '__main__':
    # dirname = askdirectory()  # show an "Open" dialog box and return the path to the selected file
    if len(sys.argv) == 2:
        dirname = sys.argv[1]
        corr_dir = os.path.join(dirname, "Leerzeichen_korrigiert")
        create_dir(corr_dir)

        for file in os.listdir(dirname):
            if file.endswith(".xlf"):
                # print(file)
                output += file + "\n\n"
                correct_spaces(os.path.join(dirname, file))
                output += "**************************\n\n"
        print_corrections()

    else:
        print("Usage: python correct_spaces.py <directory-with-xliffs-to-correct>")
