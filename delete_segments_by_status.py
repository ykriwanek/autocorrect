import correct as c
import os
from lxml import etree as ET

# paths
dirname = ""
corr_dirname = "status_deleted"

del_counter = 0

def set_paths():
    global dirname
    global corr_dirname

    dirname = c.askdirectory()  # show an "Open" dialog box and return the path to the selected file
    c.create_dir(os.path.join(dirname, corr_dirname))


def delete_file_segments(file_path, status):
    tree = ET.parse(file_path)
    root = tree.getroot()

    body = root[0][1]
    global del_counter

    for trans_unit in body.findall('{urn:oasis:names:tc:xliff:document:1.2}trans-unit'):
        state_name = trans_unit.attrib.get('{AcrossXliff}state-name')
        state_flags = trans_unit.attrib.get('{AcrossXliff}state-flags')
        if state_name == status:
            body.remove(trans_unit)
            del_counter += 1

    path, filename = os.path.split(file_path)

    if del_counter > 0:
        tree.write(os.path.join(path, corr_dirname, filename), encoding='UTF-8', xml_declaration=True)
    del_counter = 0


def delete_segments(status):
    set_paths()
    for file in os.listdir(dirname):
        if file.endswith(".xlf"):
            file_path = os.path.join(dirname, file)
            print(file_path)
            delete_file_segments(file_path, status)

if __name__ == '__main__':
    delete_segments("Final Check")