import ntpath
from pathlib import Path
from tkinter import messagebox

import correct
import os
from lxml import etree as ET
import html


def detect_blue_segments(file_path):
    file = ntpath.basename(file_path)
    blue_segments = []
    xml_segment_list = []
    if file.endswith(".xlf"):
        # print(file)
        xlf_segment_dict = get_xlf_segment_dict(file)

        # get original xml-file for this xlf-file:
        xml_file = file[:file.index(".xlf")]
        # print(correct.xml_dirname)
        # print(xml_file)
        xml_file_path = os.path.join(correct.xml_dirname, xml_file)
        xml_root = get_xml_root(xml_file_path)

        for el in xml_root.iter("*"):
            if el.tag == "chapter" or el.tag == "detailentry":
                xml_segment_list.append(el)

        counter = 0
        for id in xlf_segment_dict.keys():
            # print("*****************************")
            # print(id)
            el = xlf_segment_dict[id]
            xlf_text = ""
            for text in el.itertext("*"):
                xlf_text += text
            if counter < len(xml_segment_list):
                xml_el = xml_segment_list[counter]
                xml_greentag = ""
                if xml_el.find("greentag") is not None:
                    xml_greentag = "greentag"
                xml_text = correct.get_text(xml_el).strip()
            # print("xlf: ", xlf_text)
            # print("xml: ", xml_text, xml_el.tag, xml_greentag)

            if xml_el.tag == "chapter" or xml_greentag == "greentag":
                blue_segments.append(int(id))

            if xlf_text == xml_text:
                counter += 1
            elif xlf_text in xml_text:
                # print("xlf in xml")
                if xml_text.endswith(xlf_text):
                    counter += 1
            else:
                # print("error")
                counter += 1

    return blue_segments


def get_xlf_segment_dict(file):
    xlf_segment_dict = {}
    xlf_file_path = os.path.join(correct.dirname, file)
    xlf_root = get_root(xlf_file_path)

    for trans_unit in xlf_root.iter('trans-unit'):
        id = trans_unit.attrib.get("id")
        source = trans_unit.find('source')
        """target = trans_unit.find('target')        
        if target == None:
            xlf_segment_dict[id] = source
        else:
            xlf_segment_dict[id] = target"""
        xlf_segment_dict[id] = source

    return xlf_segment_dict


def get_root(file_path):
    tree = ET.parse(file_path)
    root = tree.getroot()
    root_str = ET.tostring(root, encoding="unicode")
    unescaped_root_str = html.unescape(root_str)
    unescaped_root = ET.fromstring(unescaped_root_str, ET.HTMLParser())
    return unescaped_root


def get_xml_root(file_path):
    try:
        f = open(file_path)
        tree = ET.parse(file_path)
        root = tree.getroot()
        return root

    except FileNotFoundError:
        print(file_path + " not found. Check the path variable and filename")
        # Error message for user
        if file_path.endswith('xml'):
            messagebox.showerror("xml-File fehlt",
                                 "Bitte den file " + os.path.basename(file_path) + " in das Verzeichnis " + str(Path(
                                     file_path).parent) +
                                 " importieren, das Verzeichnis \"Corrections\" löschen und das Skript neu starten")
            exit()

            if __name__ == '__main__':
                correct.set_paths()
            for file in os.listdir(correct.dirname):
                blue_segments = detect_blue_segments(file)
            if file.endswith("xlf"):
                print("************************")
            print(file)
            print("number of blue segments: ", len(blue_segments))
            xlf_segment_dict = get_xlf_segment_dict(file)

            for id in blue_segments:
                print(id, correct.get_text(xlf_segment_dict[str(id)]).strip())
