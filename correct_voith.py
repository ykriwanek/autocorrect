import correct
import os
import lxml
import copy

SOURCE = '{urn:oasis:names:tc:xliff:document:1.2}source'
TARGET = '{urn:oasis:names:tc:xliff:document:1.2}target'
BPT = '{urn:oasis:names:tc:xliff:document:1.2}bpt'


def correct_voith():
    correction_arts = ['format_interlief']
    correct.set_paths()
    correct.delete_corrected_files()
    if not os.path.isfile(correct.corr_file):
        correct.correct(correction_arts, checked=False)
    else:
        correct.correct(correction_arts, checked=True)
    exit()


def collect_text_children(element):
    text_children = []
    for child in element.iter():
        if correct.get_text(child).strip() != "":
            text_children.append(child)
    return text_children


def get_product(element, bpt):
    text_children = collect_text_children(element)
    product_index = text_children.index(bpt) - 1
    product_text = text_children[product_index].tail.split()
    product = product_text[len(product_text) - 1]
    return product


def correct_id(parent, source_id):
    ids = []
    # collect all ids
    for child in parent:
        child_id = child.attrib.get('id')
        ids.append(child_id)

    if source_id in ids:
        for int in range(1 - 100):
            if int not in ids:
                source_id = int
    return source_id


def __has_format_error(source, target):
    source_count = 0
    target_count = 0
    format_error = False

    if source is not None:
        for source_bpt in source.findall(BPT):
            if "Interleaf" in source_bpt.attrib.get('{AcrossXliff}s-font-name') and source_bpt.tail.strip() == 't':
                source_count += 1

    if target is not None:
        for target_bpt in target.findall(BPT):
            if "Interleaf" in target_bpt.attrib.get(
                    '{AcrossXliff}s-font-name') and target_bpt.tail.strip() == 't':
                target_count += 1

    if source_count != target_count:
        format_error = True

    return format_error


def ___correct_format(source, target):

    for source_bpt in source.findall(BPT):
        if "Interleaf" in source_bpt.attrib.get('{AcrossXliff}s-font-name') and source_bpt.tail.strip() == 't':
            product = get_product(source, source_bpt)

            for child in target:
                if child.tail is not None and product + 't' in child.tail:

                    correct.correction_counter += 1
                    split_pos = child.tail.index(product + 't') + len(product)
                    target_text_1 = child.tail[0:split_pos]
                    target_text_2 = child.tail[split_pos + 1:len(child.tail)]

                    target_bpt = copy.deepcopy(source_bpt)
                    target_ept = copy.deepcopy(source_bpt.getnext())

                    target.insert(target.index(child)+1, target_bpt)

                    child.getnext().addnext(target_ept)
                    child.getnext().getnext().tail = target_text_2
                    child.getnext().tail = 't'
                    child.tail = target_text_1


def __get_tails(tag):
    tails = ""
    for child in tag.iter():
        #print(child.tag)
        if child.tag == "{urn:oasis:names:tc:xliff:document:1.2}ph" and child.text is not None:
            tails += mark_tag_grey(child.text)
        if child.tail is not None:
            tails += mark_interlief_red(child)

    return tails


def format_interlief(body, file_path):
    across_counter = 1
    correct.append_text_to_html("Format Interlief", "corr_art")
    correct.append_corr_header_to_html()

    for trans_unit in body.findall('{urn:oasis:names:tc:xliff:document:1.2}trans-unit'):
        id = int(trans_unit.attrib.get('id'))
        source = trans_unit.find(SOURCE)
        target = trans_unit.find(TARGET)
        state_name = trans_unit.attrib.get('{AcrossXliff}state-name')
        state_flag = trans_unit.attrib.get('{AcrossXliff}state-flags')
        if __has_format_error(source, target):

            target_text = __get_tails(target)
            ___correct_format(source, target)

            correct.append_corrections_to_html("Format Interleaf", file_path, id, across_counter,
                                               __get_tails(source),
                                               target_text,
                                               __get_tails(target), "", state_name, correct.decode_flag(state_flag))


        across_counter += 1

    correct.append_corr_end_to_html()

def mark_interlief_red(tag):
    tail = tag.tail
    if tag.attrib.get('{AcrossXliff}s-font-name') == "Interleaf Symbols":
        tail = """<span style="background-color: red">""" + tail + """</span>"""

    return tail


def mark_tag_grey(text):

    text = """<span style="background-color: grey">""" + "&lt;" + text + "&gt;" + """"</span>"""

    return text


def format_interlief_checked(body, file_path, checked_units):
    across_counter = 1
    correct.append_text_to_html("Format Interlief", "corr_art")
    correct.append_corr_header_to_html()

    for trans_unit in body.findall('{urn:oasis:names:tc:xliff:document:1.2}trans-unit'):
        id = int(trans_unit.attrib.get('id'))
        source = trans_unit.find(SOURCE)
        target = trans_unit.find(TARGET)
        state_name = trans_unit.attrib.get('{AcrossXliff}state-name')
        state_flag = trans_unit.attrib.get('{AcrossXliff}state-flags')
        if file_path in checked_units.keys() and str(id) in checked_units[file_path]:
            if __has_format_error(source, target):
                target_text = __get_tails(target)
                ___correct_format(source, target)

                correct.append_corrections_to_html("Format Interleaf", file_path, id, across_counter,
                                                   __get_tails(source),
                                                   target_text,
                                                   __get_tails(target), "", state_name, correct.decode_flag(state_flag))

        across_counter += 1

    correct.append_corr_end_to_html()

if __name__ == '__main__':
    correct_voith()