#!/usr/bin/env python3

import os
from pathlib import Path
from tkinter import *
from tkinter.filedialog import askdirectory
from lxml import etree as ET
import webbrowser
import inspect
import glob
import collect_checked as coll_chkd
import correct_voith
import correct_sto

output = ""

correction_counter = 0
across_counter = 1
corrected = False
corrections2check = {}

# paths
dirname = ""
corr_dirname = ""
corr_file = ""
checked_corr_file = ""
xml_dirname = ""


# correction_arts = ["space", "capitalize", "uncapitalize", "cap_uncap_sto"]

def set_paths():
    global dirname
    global corr_dirname
    global checked_corr_file
    global corr_file
    global xml_dirname

    dirname = askdirectory()  # show an "Open" dialog box and return the path to the selected file
    corr_dirname = os.path.join(dirname, "Corrected")
    print(dirname)
    xml_dirname = os.path.join(Path(dirname).parent.parent.parent, "xml-Export")
    create_dir(corr_dirname)
    corr_file = os.path.join(dirname, "Corrected", 'corrections.htm')
    checked_corr_file = os.path.join(dirname, "Corrected", 'checked_corrections.htm')

    firefox_path = r"\\fs2\Programme\prg\firefox\firefox.exe"
    webbrowser.register('firefox', None, webbrowser.GenericBrowser(firefox_path))


def create_dir(dir_name):
    # define the name of the directory to be created
    path = dir_name

    try:
        os.mkdir(path)
    except OSError:
        print("Creation of the directory %s failed" % path)
    else:
        print("Successfully created the directory %s " % path)


# xmlns="urn:oasis:names:tc:xliff:document:1.2"
# xmlns:ax="AcrossXliff"
# xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"

def print_corrections():
    global corr_file
    global output
    out_f = open(corr_file, 'w', encoding="UTF-8")
    out_f.write(output)
    out_f.close()

    #webbrowser.get('firefox').open_new_tab('file:///' + corr_file)
    filepath='file:///' + corr_file
    webbrowser.open(filepath, new=2)


def print_checked_corrections():
    global checked_corr_file
    global output
    out_f = open(checked_corr_file, 'w+', encoding="UTF-8")
    out_f.write(output)
    out_f.close()

    webbrowser.get('firefox').open_new_tab('file:///' + checked_corr_file)


def get_lang_pair(file_path):
    tree = ET.parse(file_path)
    root = tree.getroot()

    for child in root.iter("*"):
        if child.tag == "{urn:oasis:names:tc:xliff:document:1.2}file":
            return child.attrib.get("source-language"), child.attrib.get("target-language")


def correct_file(file_path, correction_arts, checked):
    tree = ET.parse(file_path)
    root = tree.getroot()
    body = root[0][1]
    global correction_counter

    if "cap_uncap_sto" in correction_arts:
        correct_sto.cap_uncap_sto(body, file_path, checked)

    if "format_interlief" in correction_arts:
        if checked:
            checked_units = coll_chkd.collect_checked_units(corr_file)
            correct_voith.format_interlief_checked(body, file_path, checked_units)
        else:
            correct_voith.format_interlief(body, file_path)

    append_text_to_html("Insgesamt " + str(correction_counter) + " Korrekturen", "corr_number")
    append_text_to_html("---------------------------------------------------------------------------------", "file end")

    path, filename = os.path.split(file_path)
    if correction_counter > 0:
        tree.write(os.path.join(path, "Corrected", filename), encoding='UTF-8', xml_declaration=True)

    correction_counter = 0


def correct(correction_arts, checked):
    global dirname
    start_html()
    for file in os.listdir(dirname):
        if file.endswith(".xlf"):
            append_text_to_html(file, "file")
            correct_file(os.path.join(dirname, file), correction_arts, checked)
    end_html()

    if checked:
        print_checked_corrections()
    else:
        print_corrections()


def get_text(element):
    """Extract text from an element

        Args:
            element (object): The xml-element, where the text is stored

        Returns:
              text: The extracted text.
    """

    text = ""
    if element is not None:
        for child in element.iter():
            if child.text is not None:
                text += child.text
            if child.tail is not None:
                text += child.tail

    return remove_steuertags(text)


def remove_steuertags(text):
    if "<steuertag>" in text:
        splitted = text.split("<steuertag>")
        for item in splitted:
            if "</steuertag>" in item:
                steuertag_text = item.split("</steuertag>")[0]
                to_remove = "<steuertag>" + steuertag_text + "</steuertag>"
                text = text.replace(to_remove, '')

    return text


def get_comment(trans_unit):
    comment_list = []
    comment = {}
    # print(trans_unit.iter("{AcrossXliff}named-property[@name='Comment']/{AcrossXliff}named-value[@{AcrossXliff}name='Text']/text()"))
    for c in trans_unit.iter():
        if c.get('name') == "Comment":
            for nv in c.iter():
                if nv.get('{AcrossXliff}name') is not None and nv.text is not None:
                    comment[nv.get('{AcrossXliff}name')] = nv.text
            comment_list.append(comment)
            comment = {}

    return comment_list


def get_source_comment(trans_unit):
    for c in get_comment(trans_unit):
        if c["Annotates"] == "Source":
            return c


def get_target_comment(trans_unit):
    for c in get_comment(trans_unit):
        if c["Annotates"] == "Target":
            return c


def comment2text(source_comment, target_comment):
    text = ""
    if source_comment is not None:
        for key in source_comment.keys():
            # print(key)
            text += '<p>' + key + ': ' + source_comment[key] + '</p>'
    if target_comment is not None:
        for key in target_comment.keys():
            # print(key)
            text += '<p>' + key + ': ' + target_comment[key] + '</p>'
    return text


def start_html():
    global output
    output += """<html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"><title>corrections</title></head><body>"""


def append_text_to_html(text, attr_name):
    global output
    output += "<p name='" + attr_name + "'>" + text + "</p>"


def append_corr_header_to_html():
    global output
    output += """
        <table border=1>
             <tr name = 'table header'>
                <th>#</th>
               <th>Source</th>
               <th>Status</th>
               <th>Target</th>
               <th>Corrected</th>
               <th>Comment</th>
               <th>CORRECT!</th>
               
             </tr>"""


def decode_flag(state_flag):
    decoded = state_flag
    if state_flag == "8519680" or state_flag == "12713984":
        decoded = "Vorübersetzt"
    if state_flag == "79691776":
        decoded = "Kontext-Match"
    if state_flag == "4194304" or state_flag == "-2143289344" or state_flag == "0":
        decoded = ""

    return decoded


def append_corrections_to_html(corr_art, file_name, id, across_counter, source, target, corrected_text, comment, state_name,
                               state_flag):
    global output
    checked = """<input type="checkbox" value="to correct">"""
    if corrected:
        checked = """<input type="checkbox" value="to correct" checked="checked">"""

    output += """
           <tr name = 'trans-unit' corr-art = '""" + corr_art + """' file = '""" + file_name + """' id = '""" + str(
        id) + """'>
            <td name = '#'>""" + str(across_counter) + """</td>
             <td name = 'source'>""" + source + """</td>
             <td name = 'status'>""" + state_name + " (" + decode_flag(state_flag) + ")" + """</td>
             <td name = 'target'>""" + target + """</td>
             <td name = 'corrected'>""" + corrected_text + """</td>
             <td name = 'comment'>""" + comment + """</td>
             <td>
                <label>""" + checked + """</label>
             </td>   
             
           </tr>
       """


def append_corr_end_to_html():
    global output
    output += """</table>"""


def get_act_path():
    filename = inspect.getframeinfo(inspect.currentframe()).filename
    path = os.path.dirname(os.path.abspath(filename))
    return path


def end_html():
    global output

    output += """<p>
                </body>
                </html>
                """


def delete_corrected_files():
    files = glob.glob(corr_dirname + '/*.xlf')
    for f in files:
        os.remove(f)


def delete_all_files():
    files = glob.glob(corr_dirname + '/*')
    for f in files:
        os.remove(f)


def color_red(text):
    text = """<span style="background-color: red">""" + text + """</span>"""

    return text


def mark_border_sign(text):
    if text != "":
        border_sign = re.search('[\.\!\?:](\s*\u201c)?$', text)
        wo_border_sign = text[0:border_sign.start()]
        border_sign = text[border_sign.start():border_sign.end()]
        text = wo_border_sign + color_red(border_sign)
    return text


if __name__ == '__main__':
    correct_sto.correct_sto()
