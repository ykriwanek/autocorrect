import lxml.html as html

def collect_checked_units(html_file):
    tree = html.parse(html_file)
    root = tree.getroot()
    checked_units = {}
    for el in root.iter("tr"):
        if el.get("name") == "trans-unit":
            for child in el.iter():
                if child.get("checked") == "checked":
                    file_name = el.get("file")
                    id = el.get("id")
                    ids = []
                    if file_name in checked_units.keys():
                        ids = checked_units[file_name]
                    ids.append(id)
                    checked_units[file_name] = ids
    return checked_units

if __name__ == '__main__':
    collect_checked_units("file:///C:/Users/ykriwanek/Projects/filter_sto/test_files/Corrected/corrections.html")