import correct
# import nltk
import re
import os
import collect_checked
import detect_blue_segments

PROPER_NAMES = ["Sto", "Ispo", "Resol", "Sikaflex", "SolSilikat", "Südwest", "PVC",
                "Hybrid Drying Technology", "bölazinc", "wikulac", "fischer",
                "Европейск"]
ABBREVIATIONS = ["etc.", "evtl.", "itd.", "ect.", "Abb.", "Abschn.", "Alt.", "Anm.", "Aufl.", "Bsp.", "Bspw.", "Bzgl.", "ca.",
                 "D.h.", "Etc.", "Evtl.", "f.", "ff.", "ggf.", "ggf", "Hrsg.", "i.d.R.", "i. d. R.", "inkl.", "inkl",
                 "max.", "max", "min.", "min", "Mind.", "mind.", "Max.", "maks.",
                 "Maks.", "Mio.",
                 "Mrd.",
                 "Nr.",
                 "o.Ä.",
                 "o. Ä.", "Pos.", "Rd.", "S.", "s.",
                 "Tab.", "Tel.", "u.Ä.", "u. Ä.", "usw.", "usw", "v.a.", "v. a.", "z.B.", "z. B.", "z.H.", "z. H.",
                 "z.T.", "z. T.", "r.F.", "r. F."]


def starts_with_upper_word(sent):
    starts = False
    words = sent.split()
    if len(words) > 0:
        if len(words[0]) >= 1 and words[0].isupper():
            starts = True
        if "-" in words[0]:
            if len(words[0]) > 1 and words[0].split("-")[0].isupper():
                starts = True
    return starts


def starts_with_proper_name(text):
    starts = False
    for pn in PROPER_NAMES:
        if text.startswith(pn):
            starts = True

    return starts


def __ends_with_abbreviation(text):
    ends = False
    for abbr in ABBREVIATIONS:
        pattern = re.compile(r'\b' + abbr + '$')
        if re.search(pattern=pattern, string=text):
            ends = True

    return ends


def source_and_target_end_with_abbreviation(source_text, target_text):
    end = False
    if __ends_with_abbreviation(source_text) and __ends_with_abbreviation(target_text):
        end = True

    return end


def cap_uncap_sto(body, file_path):
    """Capitalize target text in all trans-units of body if
        1)The segment is blue in Across (trans-unit id is 2 more, than the id of the previous trans-unit)
        2)The source text has a dot at the end.

        Args:
            body (object): the parent element of all trans-units in the xml-file
    """

    correct.across_counter = 1
    correct.correction_counter = 0
    blue_segments = detect_blue_segments.detect_blue_segments(file_path)
    correct.append_text_to_html("Capitalize/uncapitalize Sto", "corr_art")
    correct.append_corr_header_to_html()

    for trans_unit in body.findall('{urn:oasis:names:tc:xliff:document:1.2}trans-unit'):
        id = int(trans_unit.attrib.get('id'))
        state_name = trans_unit.attrib.get('{AcrossXliff}state-name')
        state_flag = trans_unit.attrib.get('{AcrossXliff}state-flags')
        source = trans_unit.find('{urn:oasis:names:tc:xliff:document:1.2}source')
        target = trans_unit.find('{urn:oasis:names:tc:xliff:document:1.2}target')
        source_text = correct.get_text(source).strip()
        target_text = correct.get_text(target).strip()
        source_comment = correct.get_source_comment(trans_unit)
        target_comment = correct.get_target_comment(trans_unit)

        if id in blue_segments \
                or re.search('[\.\!\?:](\s*\u201c)?$', source_text) \
                or starts_with_proper_name(target_text) \
                or re.search('(^X\b|^X$)', target_text) \
                or starts_with_upper_word(target_text) \
                or comment_is_gross(source_comment):
            if target is not None and not target_text.startswith("http") and not comment_is_klein(source_comment):
                correct_target(target, "capitalize")
        else:
            if target is not None and not comment_is_gross(source_comment):
                correct_target(target, "uncapitalize")

        # get corrected target text
        corrected_target_text = ""
        if correct.corrected:
            corrected_target_text = correct.get_text(target).strip()
            correct.corrected = False
        """correct.corrections2check["Capitalize/uncapitalize Sto" + '|' + file_path + '|' + str(id)] = (
            [source_text, target_text, corrected_target_text, correct.comment2text(comment), state_name,
             state_flag])"""

        # mark segment blue if it is blue in Across
        if id in blue_segments:
            source_text = mark_blue(source_text)
            target_text = mark_blue(target_text)
            corrected_target_text = mark_blue(corrected_target_text)

        # append corrections to the report file if they were made or if there is a comment in case the file was not checked yet
        # if corrected_target_text != "" or (correct.corrected == False and correct.comment2text(comment) is not ""):
        if corrected_target_text != "":
            correct.append_corrections_to_html("Capitalize/uncapitalize Sto", file_path, id, correct.across_counter,
                                               source_text, target_text,
                                               corrected_target_text,
                                               correct.comment2text(source_comment, target_comment),
                                               state_name, state_flag)

            correct.correction_counter += 1
        prev_id = id
        correct.across_counter += 1

    correct.append_corr_end_to_html()


def mark_blue(text):
    text = """<font color="blue">""" + text + """</font>"""

    return text


def cap_uncap_sto(body, file_path, checked):
    """Capitalize target text in all trans-units of body if
        1)The segment is blue in Across (trans-unit id is 2 more, than the id of the previous trans-unit)
        2)The source text has a dot at the end.
        3)The trans-unit is marked as "checked" in the html-file

        Args:
            body (object): the parent element of all trans-units in the xml-file
            file_path (str): the path to the file to be corrected
            html_file_path (str): the path to the html-file, where proposed auto-corrections has been checked
    """

    # print(file_path)
    correct.across_counter = 1
    blue_segments = detect_blue_segments.detect_blue_segments(file_path)
    correct.append_text_to_html("Capitalize/uncapitalize Sto", "corr_art")

    # append language pair to html
    src_lang, tgt_lang = correct.get_lang_pair(file_path)
    lang_pair_text = "Source language: " + src_lang + "; Target language: " + tgt_lang
    correct.append_text_to_html(lang_pair_text, "lang_pair")

    correct.append_corr_header_to_html()

    checked_units = {}
    if checked:
        checked_units = collect_checked.collect_checked_units(correct.corr_file)

    for trans_unit in body.findall('{urn:oasis:names:tc:xliff:document:1.2}trans-unit'):
        id = int(trans_unit.attrib.get('id'))
        if checked:
            if file_path in checked_units.keys() and str(id) in checked_units[file_path]:
                __cap_uncap_trans_unit(blue_segments, trans_unit, file_path, checked)
        else:
            __cap_uncap_trans_unit(blue_segments, trans_unit, file_path, checked)

        correct.across_counter += 1
    correct.append_corr_end_to_html()


def __cap_uncap_trans_unit(blue_segments, trans_unit, file_path, checked):
    id = int(trans_unit.attrib.get('id'))
    state_name = trans_unit.attrib.get('{AcrossXliff}state-name')
    state_flag = trans_unit.attrib.get('{AcrossXliff}state-flags')
    source = trans_unit.find('{urn:oasis:names:tc:xliff:document:1.2}source')
    target = trans_unit.find('{urn:oasis:names:tc:xliff:document:1.2}target')
    source_text = correct.get_text(source).strip()
    target_text = correct.get_text(target).strip()
    source_comment = correct.get_source_comment(trans_unit)
    target_comment = correct.get_target_comment(trans_unit)

    if target is not None and not starts_with_proper_name(target_text):
        if comment_is_grossbuchstaben(source_comment):
            correct_target(target, "to_capital_letters")
        else:
            if (id in blue_segments \
                or re.search('[\.\!\?:](\s*\u201c)?$', source_text) \
                or starts_with_proper_name(target_text) \
                or re.search('(^X\b|^X$)', target_text) \
                or starts_with_upper_word(target_text) \
                or comment_is_gross(source_comment)) \
                    and not target_text.startswith("http") \
                    and not comment_is_klein(source_comment) \
                    and not __ends_with_abbreviation(source_text):

                correct_target(target, "capitalize")

            else:
                correct_target(target, "uncapitalize")

    # get corrected target text
    corrected_target_text = ""
    if correct.corrected:
        corrected_target_text = correct.get_text(target).strip()

        """correct.corrections2check["Capitalize/uncapitalize Sto" + '|' + file_path + '|' + str(id)] = (
            [source_text, target_text, corrected_target_text, correct.comment2text(comment), state_name,
             state_flag])"""

    # append corrections to the report file if they were made or if there is a comment in case the file was not checked yet
    # if corrected_target_text != "" or (correct.corrected == False and correct.comment2text(comment) is not ""):
    if (correct.corrected or (not checked and (correct.comment2text(source_comment, target_comment) is not ""
                                               and not (target_comment is None and comment_is_klein(source_comment))
                                               and not (target_comment is None and comment_is_gross(source_comment))
                                               and not (
                    target_comment is None and comment_is_grossbuchstaben(
                source_comment))))) and state_name != "Verified":

        # count real corrections
        if correct.corrected:
            correct.correction_counter += 1

        # mark segment blue if it is blue in Across
        if id in blue_segments:
            source_text = mark_blue(source_text)
            target_text = mark_blue(target_text)
            corrected_target_text = mark_blue(corrected_target_text)

        # mark border sign if it differs in source and target
        if __border_sign_only_in_source(source_text, target_text):
            source_text = correct.mark_border_sign(source_text)

        if __border_sign_only_in_target(source_text, target_text):
            target_text = correct.mark_border_sign(target_text)
            corrected_target_text = correct.mark_border_sign(corrected_target_text)

        # append all necessary elements to html
        correct.append_corrections_to_html("Capitalize/uncapitalize Sto", file_path, id, correct.across_counter,
                                           source_text, target_text,
                                           corrected_target_text, correct.comment2text(source_comment, target_comment),
                                           state_name, state_flag)

    correct.corrected = False


def __border_sign_only_in_source(source_text, target_text):
    differs = False
    if re.search('[\.\!\?:](\s*\u201c)?$', source_text) and not re.search('[\.\!\?:](\s*\u201c)?$', target_text):
        differs = True
    return differs


def __border_sign_only_in_target(source_text, target_text):
    differs = False
    if re.search('[\.\!\?:](\s*\u201c)?$', target_text) and not re.search('[\.\!\?:](\s*\u201c)?$', source_text):
        differs = True
    return differs


def comment_is_klein(comment):
    is_klein = False
    # print("comment:", comment)
    if comment is not None and comment["Annotates"] == "Source":
        if "Title" in comment.keys() and comment["Title"].lower().strip() == "klein":
            is_klein = True
        if "Text" in comment.keys() and comment["Text"].lower().strip() == "klein":
            is_klein = True
    return is_klein


def comment_is_gross(comment):
    is_gross = False
    if comment is not None and comment["Annotates"] == "Source":
        if "Title" in comment.keys() and (
                comment["Title"].lower().strip() == "gross" or comment["Title"].lower().strip() == "groß"):
            is_gross = True
        if "Text" in comment.keys() and (
                comment["Text"].lower().strip() == "gross" or comment["Text"].lower().strip() == "groß"):
            is_gross = True
    return is_gross


def comment_is_grossbuchstaben(comment):
    is_grossbuchstaben = False
    if comment is not None and comment["Annotates"] == "Source":
        if "Title" in comment.keys() and (comment["Title"].lower().strip() == "grossbuchstaben" or comment[
            "Title"].lower().strip() == "großbuchstaben"):
            is_grossbuchstaben = True
        if "Text" in comment.keys() and (comment["Text"].lower().strip() == "grossbuchstaben" or comment[
            "Text"].lower().strip() == "großbuchstaben"):
            is_grossbuchstaben = True
    return is_grossbuchstaben


def correct_target(target, correction_art):
    """Extract text from target and change it according to correction art

        Args:
            target (object): The xml-element, where translation text is stored
            correction_art (str): the correction art - predefined in the list 'correction_arts"
    """
    if correction_art in ["capitalize", "uncapitalize", "to_capital_letters"]:
        corr = False

        for child in target.iter():
            if corr == False:
                if child.tail is not None and re.search('\S', child.tail):
                    child.tail = __correct_text(child.tail, correction_art)
                    corr = True

                if child.text is not None and re.search('\S', child.text):
                    child.text = __correct_text(child.text, correction_art)
                    corr = True



    else:
        for child in target.iter():
            if child.tail is not None:
                child.tail = __correct_text(child.tail, correction_art)
            if child.text is not None:
                child.text = __correct_text(child.text, correction_art)


def __correct_text(text, correction_art):
    """Change string according to correction art

    Args:
        text (str): The string to be changed
        correction_art (str): the correction art - predefined in the list 'correction_arts"

    Returns:
          corr_text: The changed string.
    """
    corr_text = text

    if correction_art == "capitalize":
        if text[0].islower():
            corr_text = __capitalize(text)
            correct.corrected = True

    if correction_art == "uncapitalize":
        if text[0].isupper():
            corr_text = __uncapitalize(text)
            correct.corrected = True

    if correction_art == "to_capital_letters":
        if not text.isupper():
            corr_text = text.upper()
            correct.corrected = True

    return corr_text


# change the first character of a string to upper case
def __capitalize(text):
    return text.capitalize()


# change the first character of a string to lower case
def __uncapitalize(text):
    text = text[0].lower() + text[1:]
    return text


def correct_sto():
    correction_arts = ['cap_uncap_sto']
    correct.set_paths()
    correct.delete_corrected_files()
    if not os.path.isfile(correct.corr_file):
        correct.correct(correction_arts, checked=False)
    else:
        correct.correct(correction_arts, checked=True)
    exit()
