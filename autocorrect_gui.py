from tkinter import *
from tkinter import ttk
import correct_sto
import delete_segments_by_status as dsbs
import correct_voith

master = Tk()
master.title('Auto-corrections')

f = ttk.LabelFrame(master)
f.pack()

correct_sto_button = ttk.Button(f, text="Correct Sto", command=lambda: correct_sto.correct_sto())
correct_sto_button.pack(padx=10, pady=10)

delete_kontext_match_status_button = ttk.Button(f, text="Delete segments with status <Final Check>", command=lambda: dsbs.delete_segments("Final Check"))
delete_kontext_match_status_button.pack(padx=10, pady=10)

correct_voith_button = ttk.Button(f, text="Correct Interlief-formatting by Voith idml-texts", command=lambda: correct_voith.correct_voith())
correct_voith_button.pack(padx=10, pady=10)

mainloop()
