import correct
# import nltk
import re
import os


def starts_with_upper_word(sent):
    starts = False
    words = sent.split()
    if len(words) > 0 and words[0].isupper():
        print(words[0])
        starts = True
    if len(words) > 0 and "-" in words[0]:
        if words[0].split("-")[0].isupper():
            starts = True
    return starts


def cap_uncap_sto(body, file_path):
    """Capitalize target text in all trans-units of body if
        1)The segment is blue in Across (trans-unit id is 2 more, than the id of the previous trans-unit)
        2)The source text has a dot at the end.

        Args:
            body (object): the parent element of all trans-units in the xml-file
    """

    across_counter = 1
    prev_id = 2
    correct.append_text_to_html("Capitalize/uncapitalize Sto", "corr_art")
    correct.append_corr_header_to_html()

    for trans_unit in body.findall('{urn:oasis:names:tc:xliff:document:1.2}trans-unit'):
        id = int(trans_unit.attrib.get('id'))
        state_name = trans_unit.attrib.get('{AcrossXliff}state-name')
        state_flag = trans_unit.attrib.get('{AcrossXliff}state-flags')
        source = trans_unit.find('{urn:oasis:names:tc:xliff:document:1.2}source')
        target = trans_unit.find('{urn:oasis:names:tc:xliff:document:1.2}target')
        source_text = correct.get_text(source).strip()
        target_text = correct.get_text(target).strip()
        comment = correct.get_comment(trans_unit)

        if (id - prev_id) == 2 \
                or re.search('[\.\!\?](\s*\u201c)?$', source_text) \
                or target_text.startswith("Sto") \
                or target_text.startswith('Ispo') \
                or target_text.startswith('Resol') \
                or re.search('(^X\b|^X$)', target_text) \
                or starts_with_upper_word(target_text) \
                or comment_is_gross(comment):
            if target is not None and not target_text.startswith("http") and not comment_is_klein(comment):
                correct_target(target, "capitalize")
        else:
            if target is not None and not comment_is_gross(comment):
                correct_target(target, "uncapitalize")

        #get corrected target text
        corrected_target_text = ""
        if correct.corrected:
            corrected_target_text = correct.get_text(target).strip()

        correct.correction_counter += 1
        correct.corrected = False
        """correct.corrections2check["Capitalize/uncapitalize Sto" + '|' + file_path + '|' + str(id)] = (
            [source_text, target_text, corrected_target_text, correct.comment2text(comment), state_name,
             state_flag])"""

        # mark segment blue if it is blue in Across
        if (id - prev_id) == 2:
            source_text = mark_blue(source_text)
            target_text = mark_blue(target_text)
            corrected_target_text = mark_blue(corrected_target_text)

        #append corrections to the report file if they were made or if there is a comment in case the file was not checked yet
        #if corrected_target_text != "" or (correct.corrected == False and correct.comment2text(comment) is not ""):
        if corrected_target_text != "":
            correct.append_corrections_to_html("Capitalize/uncapitalize Sto", file_path, id, across_counter,
                                               source_text, target_text,
                                               corrected_target_text, correct.comment2text(comment),
                                               state_name, state_flag)


        prev_id = id
        across_counter += 1
    correct.append_corr_end_to_html()


def mark_blue(text):
    text = """<font color="blue">""" + text + """</font>"""

    return text



def cap_uncap_sto(body, file_path):
    """Capitalize target text in all trans-units of body if
        1)The segment is blue in Across (trans-unit id is 2 more, than the id of the previous trans-unit)
        2)The source text has a dot at the end.

        Args:
            body (object): the parent element of all trans-units in the xml-file
    """

    across_counter = 1
    prev_id = 2
    correct.append_text_to_html("Capitalize/uncapitalize Sto", "corr_art")
    correct.append_corr_header_to_html()

    for trans_unit in body.findall('{urn:oasis:names:tc:xliff:document:1.2}trans-unit'):
        id = int(trans_unit.attrib.get('id'))
        state_name = trans_unit.attrib.get('{AcrossXliff}state-name')
        state_flag = trans_unit.attrib.get('{AcrossXliff}state-flags')
        source = trans_unit.find('{urn:oasis:names:tc:xliff:document:1.2}source')
        target = trans_unit.find('{urn:oasis:names:tc:xliff:document:1.2}target')
        source_text = correct.get_text(source).strip()
        target_text = correct.get_text(target).strip()
        comment = correct.get_comment(trans_unit)

        if (id - prev_id) == 2 \
                or re.search('[\.\!\?](\s*\u201c)?$', source_text) \
                or target_text.startswith("Sto") \
                or target_text.startswith('Ispo') \
                or target_text.startswith('Resol') \
                or re.search('(^X\b|^X$)', target_text) \
                or starts_with_upper_word(target_text) \
                or comment_is_gross(comment):
            if target is not None and not target_text.startswith("http") and not comment_is_klein(comment):
                correct_target(target, "capitalize")
        else:
            if target is not None and not comment_is_gross(comment):
                correct_target(target, "uncapitalize")

        #get corrected target text
        corrected_target_text = ""
        if correct.corrected:
            corrected_target_text = correct.get_text(target).strip()
            correct.correction_counter += 1

        """correct.corrections2check["Capitalize/uncapitalize Sto" + '|' + file_path + '|' + str(id)] = (
            [source_text, target_text, corrected_target_text, correct.comment2text(comment), state_name,
             state_flag])"""

        # mark segment blue if it is blue in Across
        if (id - prev_id) == 2:
            source_text = mark_blue(source_text)
            target_text = mark_blue(target_text)
            corrected_target_text = mark_blue(corrected_target_text)

        #append corrections to the report file if they were made or if there is a comment in case the file was not checked yet
        #if corrected_target_text != "" or (correct.corrected == False and correct.comment2text(comment) is not ""):
        if correct.corrected or correct.comment2text(comment) is not "":
            correct.append_corrections_to_html("Capitalize/uncapitalize Sto", file_path, id, across_counter,
                                               source_text, target_text,
                                               corrected_target_text, correct.comment2text(comment),
                                               state_name, state_flag)

        correct.corrected = False

        prev_id = id
        across_counter += 1
    correct.append_corr_end_to_html()


def comment_is_klein(comment):
    is_klein = False
    # print("comment:")
    if not comment == {} and comment["Annotates"] == "Source" and comment["Title"].lower().strip() == "klein":
        is_klein = True
    return is_klein


def comment_is_gross(comment):
    is_gross = False

    if not comment == {} and comment["Annotates"] == "Source" and (
            comment["Title"].lower().strip() == "gross" or comment["Title"].lower().strip() == "groß"):
        is_gross = True
    return is_gross


def correct_target(target, correction_art):
    """Extract text from target and change it according to correction art

        Args:
            target (object): The xml-element, where translation text is stored
            correction_art (str): the correction art - predefined in the list 'correction_arts"
    """
    if correction_art == "capitalize" or correction_art == "uncapitalize":
        corr = False

        for child in target.iter():

            if corr == False:
                if child.tail is not None and re.search('\S', child.tail):
                    child.tail = __correct_text(child.tail, correction_art)
                    corr = True

                if child.text is not None and re.search('\S', child.text):
                    child.text = __correct_text(child.text, correction_art)
                    corr = True



    else:
        for child in target.iter():
            if child.tail is not None:
                child.tail = __correct_text(child.tail, correction_art)
            if child.text is not None:
                child.text = __correct_text(child.text, correction_art)


def __correct_text(text, correction_art):
    """Change string according to correction art

    Args:
        text (str): The string to be changed
        correction_art (str): the correction art - predefined in the list 'correction_arts"

    Returns:
          corr_text: The changed string.
    """
    corr_text = text

    if correction_art == "capitalize":
        if text[0].islower():
            corr_text = __capitalize(text)
            correct.corrected = True

    if correction_art == "uncapitalize":
        if text[0].isupper():
            corr_text = __uncapitalize(text)
            correct.corrected = True
    return corr_text


# change the first character of a string to upper case
def __capitalize(text):
    return text.capitalize()


# change the first character of a string to lower case
def __uncapitalize(text):
    text = text[0].lower() + text[1:]
    return text


def correct_sto():
    correction_arts = ['cap_uncap_sto']
    correct.set_paths()
    correct.delete_corrected_files()
    if not os.path.isfile(correct.corr_file):
        correct.correct(correction_arts, False)
    else:
        correct.correct(correction_arts, True)
    exit()
